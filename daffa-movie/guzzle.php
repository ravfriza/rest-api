<?php
require 'vendor/autoload.php';

use GuzzleHttp\Client;

$client = new Client();

$response = $client->request('GET', 'http://omdbapi.com', [
    'query' => [
        'apikey' => 'd4d0a04d',
        's' => 'harry potter'
    ]
]);

$result = json_decode($response->getBody()->getContents(), true);
$movTitle = $result['Search'][0]['Title'];
$released = $result['Search'][0]['Year'];
$poster = $result['Search'][0]['Poster'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php foreach ($result['Search'] as $mov) : ?>
        <ul>
            <li>Title: <?= $mov['Title']; ?></li>
            <li>Released: <?= $mov['Year']; ?></li>
            <li>
                <img src="<?= $mov['Poster']; ?>">
            </li>
        </ul>
    <?php endforeach; ?>
</body>

</html>