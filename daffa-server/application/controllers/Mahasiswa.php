<?php

use chriskacerguis\RestServer\RestController;

class Mahasiswa extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mahasiswa_model', 'mhs');
        $this->methods['index_get']['limit'] = 100;
        $this->methods['index_post']['limit'] = 100;
        $this->methods['index_delete']['limit'] = 100;
        $this->methods['index_put']['limit'] = 100;
    }


    public function index_get()
    {
        $id = $this->get('id');
        $mhs = $this->mhs->get($id);

        if ($mhs) {
            $this->response(['status' => true, 'data' => $mhs], RestController::HTTP_OK);
        } else {
            $this->response(['status' => false, 'msg' => 'id not found'], RestController::HTTP_NOT_FOUND);
        }
    }

    public function index_delete()
    {
        $id = $this->delete('id');

        if ($id == null) {
            $this->response(['status' => false, 'msg' => 'provide an id'], RestController::HTTP_BAD_REQUEST);
        } else {
            if ($this->mhs->delete($id) > 0) {
                // ok
                $this->response(['status' => true, 'id' => $id, 'msg' => 'delete success'], RestController::HTTP_OK);
            } else {
                // id not found
                $this->response(['status' => false, 'msg' => 'id not found'], RestController::HTTP_NOT_FOUND);
            }
        }
    }

    public function index_post()
    {
        $data = [
            'nrp' => $this->post('nrp'),
            'nama' => $this->post('nama'),
            'email' => $this->post('email'),
            'jurusan' => $this->post('jurusan')
        ];

        if ($this->mhs->create($data) > 0) {
            $this->response(['status' => true, 'msg' => 'create success'], RestController::HTTP_CREATED);
        } else {
            $this->response(['status' => false, 'msg' => 'create failed'], RestController::HTTP_BAD_REQUEST);
        }
    }

    public function index_put()
    {
        $id = $this->put('id');
        $data = [
            'nrp' => $this->put('nrp'),
            'nama' => $this->put('nama'),
            'email' => $this->put('email'),
            'jurusan' => $this->put('jurusan')
        ];

        if ($this->mhs->update($data, $id) > 0) {
            $this->response(['status' => true, 'msg' => 'update success'], RestController::HTTP_OK);
        } else {
            $this->response(['status' => false, 'msg' => 'update failed'], RestController::HTTP_BAD_REQUEST);
        }
    }
}
