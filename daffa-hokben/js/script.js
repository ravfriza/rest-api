function TampilkanSemuaMenu() {
    $.getJSON('data/menu.json', function(result) {
    let menu = result.menu;
    $('#daftar-menu').empty(); 
    $.each(menu, function(i, data) {
        $('#daftar-menu').append('<div class="col-md-4"><div class="card mb-3"><img src="img/' + data.gambar + '" class="card-img-top"><div class="card-body"><h5 class="card-title">' + data.nama + '</h5><p class="card-text">Some quick example text to build on the card title and make up the bulk of the cards content.</p><h3>Rp. ' + data.harga + '</h3><a href="#" class="btn btn-primary">Pesan Sekarang</a></div></div></div>')
    });
});
}

TampilkanSemuaMenu();

$('.nav-link').on('click', function() {
    $('.nav-link').removeClass('active');
    $(this).addClass('active');

    let kategori = $(this).html();
    $('h1').html(kategori);

    if(kategori == 'All Menu'){
        TampilkanSemuaMenu();
        return;
    }

$.getJSON('data/menu.json', function (result) {
    let menu = result.menu;
    let content = '';

    $.each(menu, function(i, data) {
        if(data.kategori == kategori) {
            content += '<div class="col-md-4"><div class="card mb-3"><img src="img/' + data.gambar + '" class="card-img-top"><div class="card-body"><h5 class="card-title">' + data.nama + '</h5><p class="card-text">Some quick example text to build on the card title and make up the bulk of the cards content.</p><h3>Rp. ' + data.harga + '</h3><a href="#" class="btn btn-primary">Pesan Sekarang</a></div></div></div>';
        }
        });
            $('#daftar-menu').html(content);
    });

});